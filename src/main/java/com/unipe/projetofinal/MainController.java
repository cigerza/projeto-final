package com.unipe.projetofinal;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import net.rgielen.fxweaver.core.FxmlView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Component
@FxmlView("main-stage.fxml")
public class MainController {

	@FXML
    private TextField email;
    @FXML
    private TextField senha;    
    @FXML
    private Button botao;
    @FXML
    private Hyperlink link;
    
    @FXML    
    private Label cadastroLabel;
    private Label entrarLabel;
    private UsuarioService userService;

    @Autowired
    private UsuarioRepository usuarioRepository;
    	
    @PostMapping(path="/cadastro")
    public @ResponseBody String addNewUser (@RequestParam String nome, @RequestParam String email, @RequestParam String senha) {
    	Usuario n = new Usuario();
    	n.setNome(nome);
    	n.setEmail(email);
    	n.setSenha(senha);
    	usuarioRepository.save(n);
    	return "Cadastrado";
    }        
    
    public void loadInicial(ActionEvent actionEvent) {
        this.entrarLabel.setText(userService.getText());
    	//this.entrarLabel.setOnMouseClicked(userService.getText());
    }

    public void loadCadastro(ActionEvent actionEvent) {
        this.cadastroLabel.setText("Cadastre-se!");
    }

}
